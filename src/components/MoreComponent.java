package components;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import pages.BasePage;
import utils.CommonUtils;
import utils.SnapshotUtil;

public class MoreComponent extends BasePage{

	public MoreComponent() {
		// TODO Auto-generated constructor stub
	}

	public WebElement getMoreMenu(){
		WebElement menuMore = driver.findElement(By.xpath("//*[@id='opsbar-operations_more_drop']/div"));
		return menuMore;
	}
	//*[@id="log-work"]/a/span
	
	public WebElement getLinkLogWork(){
		WebElement logWork = driver.findElement(By.xpath("//*[@id='log-work']/a/span"));
		return logWork;
	}
	public void waitForPageToLoad() { 
		waitForPageToLoad(this.getMoreMenu());
		Reporter.log(this.getClass().getName()+" loaded");
		SnapshotUtil.takeSnapshot(this.getClass().getName()+CommonUtils.getDTString()+".jpeg");
	}
	
	//doesn't work on inner list
	public void selectMenuItem(String menuItem) {		
		//String xpath = CommonUtils.extractxPath(getMoreMenu()); //incorrect xpath
		//CommonUtils.selectByVisibleTextFromlist(xpath, menuItem);
		List<WebElement> welist = CommonUtils.getListItems("//*[@id='opsbar-operations_more_drop']/div");
		CommonUtils.selectByTextFromlist(welist, menuItem);
		
	}
	
	///html/body/aui-dropdown-menu
}
