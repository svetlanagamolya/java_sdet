package components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import pages.BasePage;
import pages.MainPage;
import utils.CommonUtils;
import utils.SnapshotUtil;

public class TimesheetComponent extends BasePage{

	public TimesheetComponent() {
		// TODO Auto-generated constructor stub
	}
	
	public WebElement getLinkMyTimesheet(){
		WebElement timesheet = driver.findElement(By.xpath("//a[@id='my-timesheet-report-link_lnk']"));
		return timesheet;
	}
	
	public WebElement getHeaderReports(){
		WebElement headerReports = driver.findElement(By.xpath("//div[@id='timesheet-section']/strong[contains(text(), 'Reports')]"));
		return headerReports;
	}
	
	public WebElement getButtonTimesheet(){
		WebElement timesheet = driver.findElement(By.xpath("//a[@id='timesheet-main-link']"));
		return timesheet;
	}
	
	public void waitForPageToLoad() { 
		waitForPageToLoad(getHeaderReports()); 
		Reporter.log(this.getClass().getName()+" loaded");
		SnapshotUtil.takeSnapshot(this.getClass().getName()+CommonUtils.getDTString()+".jpeg");
	}

	
}
