package components;

import static org.testng.Assert.assertTrue;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;

import pages.BasePage;
import pages.IssuePage;
import utils.CommonUtils;
import utils.SnapshotUtil;

public class LogWorkComponent extends BasePage{

	public LogWorkComponent() {
		// TODO Auto-generated constructor stub
	}

	//*[@id="log-work-dialog"]
	public WebElement getComponent(){
		WebElement logworkComponent = driver.findElement(By.xpath("//*[@id='log-work-dialog']"));
		return logworkComponent;
	}

	public void waitForPageToLoad() { 
		waitForPageToLoad(this.getComponent());
		Reporter.log(this.getClass().getName()+" loaded<br>");
		SnapshotUtil.takeSnapshot(this.getClass().getName()+CommonUtils.getDTString()+".jpeg");
	}

	public WebElement getTextboxTimeSpent(){
		WebElement textboxTimeSpent = driver.findElement(By.xpath("//*[@id='log-work-time-logged']"));
		return textboxTimeSpent;
	}

	public WebElement getTextboxDateStarted(){
		WebElement textboxDateStarted = driver.findElement(By.xpath("//*[@id='log-work-date-logged-date-picker']"));
		return textboxDateStarted;
	}

	public WebElement getCalendarIcon(){
		WebElement calendarIcon = driver.findElement(By.xpath("//*[@id='log-work-date-logged-icon']"));
		return calendarIcon;
	}

	private WebElement getCalendar(){
		WebElement calendar = driver.findElement(By.xpath("//div[@class='calendar active']"));
		return calendar;
	}


	public void waitForCalendarToLoad() {
		waitForPageToLoad(this.getCalendar());
		Reporter.log(this.getClass().getName()+" loaded");
		SnapshotUtil.takeSnapshot(this.getClass().getName()+CommonUtils.getDTString()+".jpeg");
	}

	public WebElement getDateOnCalendar(String date){
		WebElement dateOnCalendar = driver.findElement(By.xpath("//div[@class='calendar active']/table/tbody/*/td[contains(@class,'day day') and not(contains(@class,'weekend'))and text()="+date+"]"));
		return dateOnCalendar;
	}


	public void selectDate(String date) {
		waitForCalendarToLoad();
		this.getCalendarIcon().click();
		try {

			getDateOnCalendar(date).click();
			System.out.println(date + " is a weekday and selected");
		} catch (Exception e) {

			//e.printStackTrace();
			System.out.println(date + " is a weekend and NOT selected");
		}
	}


/* not needed */ 
	public WebElement getCalendarTimeHour(){
		WebElement timeHour = driver.findElement(By.xpath("//div[@class='calendar active']/table/tbody/*/td[@class='time']/span[@class='hour']"));
		return timeHour;
	}
	/* not needed */ 
	public WebElement getCalendarTimeMinute(){
		WebElement timeHour = driver.findElement(By.xpath("//div[@class='calendar active']/table/tbody/*/td[@class='time']/span[@class='minute']"));
		return timeHour;
	}
	/* not needed */ 
	public WebElement getCalendarTimeAMPM(){
		WebElement timeHour = driver.findElement(By.xpath("//div[@class='calendar active']/table/tbody/*/td[@class='time']/span[@class='ampm']"));
		return timeHour;
	}


	public WebElement getTextBoxWorkDescription(){
		WebElement workDescription = driver.findElement(By.xpath("//*[@id='log-work']/div[@class='form-body']/fieldset/div[@class='field-group comment-input']/textarea"));
		return workDescription;
	}
	
	public WebElement getButtonLog(){
		WebElement buttonLog = driver.findElement(By.xpath("//*[@id='log-work-submit']"));
		return buttonLog;
	}
	
	public WebElement getMessageSuccess(){
		WebElement messageSuccess = driver.findElement(By.xpath("//*[@id='aui-flag-container']/div/div"));
		return messageSuccess;
	}
	
	
	/*
	 * to fill in entire log work for a month, starting date given in properties
	 */
	public void fillInLogWorkForMonth() {
		//Started Date and Month from Properties file
		//loop through till last day of the month
		int startedDateDate = Integer.parseInt(getPropertiesFile().getDateStartedDate().toString());
		int startedDateMonth = Integer.parseInt(getPropertiesFile().getDateStartedMonth());	
		int daysInMonth = (LocalDateTime.of(2020, startedDateMonth, startedDateDate, 9, 10)).getMonth().maxLength();
		//int daysInMonth = 5;
		
		while (startedDateDate <= daysInMonth) {
			
			//wait.until(ExpectedConditions.elementToBeClickable(issuePage.getButtonMore()));
			//wait.until(ExpectedConditions.visibilityOf(issuePage.getButtonMore()));
			
			
			LocalDateTime datetimeStarted = LocalDateTime.of(2020, startedDateMonth, startedDateDate, 9, 10);
			DateTimeFormatter myFormat = DateTimeFormatter.ofPattern("dd/MMM/yy hh:mm a");
			String formattedDate = datetimeStarted.format(myFormat); //converting to expected format accepted by Jira

			if (datetimeStarted.getDayOfWeek() == DayOfWeek.SATURDAY || datetimeStarted.getDayOfWeek() == DayOfWeek.SUNDAY) {
				System.out.println(formattedDate + " is " +datetimeStarted.getDayOfWeek() + " and NOT  selected");
				Reporter.log(formattedDate + " is " +datetimeStarted.getDayOfWeek() + " and NOT  selected<br>");
			} else {
				driver.navigate().refresh(); //reload page to get rid of Success pop up message
				driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
				
				IssuePage issuePage = new IssuePage(); 
				
				issuePage.waitForPageToLoad();
				issuePage.getLinkLogWork().click();
				/*
				issuePage.getButtonMore().click();
				
				MoreComponent menuMore = new MoreComponent(); 
				menuMore.waitForPageToLoad();
				wait.until(ExpectedConditions.elementToBeClickable(menuMore.getLinkLogWork()));
				menuMore.getLinkLogWork().click(); */
				
				this.waitForPageToLoad();
				this.getTextboxTimeSpent().sendKeys("8h");
				
				this.getTextboxDateStarted().clear();
				this.getTextboxDateStarted().sendKeys(formattedDate);
				
				this.getTextBoxWorkDescription().sendKeys("work on Zelle4 project");
				this.getButtonLog().click();
				try {
					wait.until(ExpectedConditions.invisibilityOf(getMessageSuccess()));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					SnapshotUtil.takeSnapshot("SM_notDispl.jpeg");
				}
				Reporter.log(startedDateDate + " is weekday" + datetimeStarted.getDayOfWeek()+"and is  selected<br>");
			}
			startedDateDate++;
			
			
			
			
		}


		//System.out.println("formatted date: " +formattedDate);

		//Calendar calendar = Calendar.getInstance();
		//DayOfWeek dow = datetimeStarted.getDayOfWeek();




		/*
		 * System.out.println("date time of: " +LocalDateTime.of(2020, startedDateMonth,
		 * startedDateDate, 9, 10));
		 * 
		 * System.out.println("dow compare to "+ dow.compareTo(DayOfWeek.SATURDAY));
		 * System.out.println("dow "+ dow);
		 */


	}

}

