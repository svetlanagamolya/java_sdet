package utils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


import tests.BaseTest;

public class CommonUtils {

	static WebDriver underlyingDriver = BaseTest.getDriver();

	public CommonUtils() {
		// TODO Auto-generated constructor stub
	}




	private static LocalDateTime datetime = LocalDateTime.now();
	//public static String dt = datetime.toString();

	public static String getDTString() {
		String dt = datetime.toString();
		return dt;
	}

	public static List<WebElement> getListItems(String xpath){
		List<WebElement> listItems = underlyingDriver.findElements(By.xpath(xpath));
		int i = 0;
		for(WebElement e : listItems) {
			System.out.println("List Item: " + i +e.getText());
			//System.out.println("list size: "+e.getSize());
			i ++; 
		}
		return listItems;
	}

	public static void selectByVisibleTextFromlist(String listXpath, String item) {
		Select projects = new Select (underlyingDriver.findElement(By.xpath(listXpath)));
		/*
		 * You need to update your locator to get the <select> element (within that <div>. 
		 * A SelectElement object can only be created from a <select>.
		 */
		try {
			projects.selectByVisibleText(item);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void selectByTextFromlist(List<WebElement> listWebElements, String item) {

		/*
		 * for (WebElement opt : listWebElements) {
		 * System.out.println("element to String = " + opt.toString()); 
		 * if
		 * (opt.getText().equals(item)) { opt.click();
		 * 
		 * } else { System.err.println("Element "+item+ " not found"); }
		 * 
		 * }
		 */
		//List<WebElement> links = driver.find elements(By.cssSelector('#tblPriorEventsBody td a'));

		Iterator<WebElement> iter = listWebElements.iterator();

		while(iter.hasNext()) {
			WebElement webElement = iter.next();
			System.out.println("webelement text " + webElement.getText());


			if (webElement.getText().equals(item)) { 
				webElement.click(); // do something
			 } else { System.err.println("Element "+item+ " not found"); }

		}
	}

	public static String extractxPath(WebElement webelement) {
		//returns strings as - //*[@id='opsbar-operations_more_drop']/div]  - with appended last char ']' 
		String[] a = webelement.toString().split("xpath:");
		String xpathString = StringUtils.chop(a[a.length-1]);
		System.out.println("extracted xpath = "+ xpathString);
		//StringUtils.chop(xpathString);
		return xpathString;
	}


	public static String createSubDir(String path, String dir) throws Exception {
		// create the browser folder

		File subDir = new File(path /* + File.separator */ + dir);
		if (!subDir.exists()) {
			subDir.mkdir();
		}
		return path /* + File.separator */ + dir;
	}
}
