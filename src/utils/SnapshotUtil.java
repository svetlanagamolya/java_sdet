package utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import tests.BaseTest;


public class SnapshotUtil{

	static WebDriver underlyingDriver = BaseTest.getDriver();

	public static void takeSnapshot(String filename){



		//Convert web driver object to TakeScreenshot

		TakesScreenshot scrShot =((TakesScreenshot)underlyingDriver);

		//Call getScreenshotAs method to create image file

		File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

		//Move image file to new destination

		//File DestFile=new File("/Users/infoneer31infostretch/eclipse-workspace/Java_test/src/screenshots/scr2.jpeg");
		File DestFile=new File("/Users/infoneer31infostretch/eclipse-workspace/Java_test/src/screenshots/", filename);

		//Copy file at destination

		try {
			FileUtils.copyFile(SrcFile, DestFile);
			Reporter.log("snapshot taken"); 
		} catch (IOException e) {
			Reporter.log("error in taking snapshot");  
			e.printStackTrace();
		}


	}
}


