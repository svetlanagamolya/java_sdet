
package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFileUtils {
	Properties prop = new Properties();
	
	public void loadPropertiesFile() {
		
	
	File file = new File("/Users/infoneer31infostretch/eclipse-workspace/Java_test/src/utils/data.properties");
	  
	FileInputStream fileInput = null;
	
	try {
		fileInput = new FileInputStream(file);
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}

	
	//load properties file
	try {
		prop.load(fileInput);
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	

}
	
	public String  getURL() {
		return prop.getProperty("URL");
	}
	
	public String  getUsername() {
		return prop.getProperty("Username");
	}
	
	public String  getPassword() {
		return prop.getProperty("Password");
	}
	
	public String  getStartDate() {
		return prop.getProperty("StartDate");
	}
	
	public String  getEndDate() {
		return prop.getProperty("EndDate");
	}
	
	public String  getDateStartedDate() {
		return prop.getProperty("DateStartedDate");
	}
	
	public String  getDateStartedMonth() {
		return prop.getProperty("DateStartedMonth");
	}
	
	public String  getDateStartedYear() {
		return prop.getProperty("DateStartedYear");
	}
	
	public String  getDateStartedHour() {
		return prop.getProperty("DateStartedHour");
	}
	
}
