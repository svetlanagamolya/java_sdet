package tests;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import components.TimesheetComponent;
import pages.LoginPage;
import pages.MainPage;
import pages.ConfigureMyTimesheetPage;
import utils.CommonUtils;
import utils.SnapshotUtil;

public class LoginTest extends BaseTest{

	//@Test (priority = 0) 
	public void test_InvalidLogin() { 
		String expected_err = "Sorry, your username and password are incorrect - please try again.";

		LoginPage loginPage = new LoginPage(); 
		loginPage.waitForPageToLoad();
		loginPage.getTextbox_username().sendKeys("ivalidusername");
		loginPage.getTextbox_password().sendKeys("invalidpassword");
		loginPage.getButton_login().click();
		getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

		SnapshotUtil snapshot = new SnapshotUtil(); 
		snapshot.takeSnapshot("invalid login");

		Assert.assertEquals(loginPage.getError_invalidLogin().getText().toString(), expected_err);
		Reporter.log("test complete");



	}

	//@Test (priority = 0) 
	public void test_Login() { 
		LoginPage loginPage = new LoginPage(); 
		loginPage.waitForPageToLoad();
		loginPage.getLogo().isDisplayed();
		loginPage.signOnLoginPage();

		SnapshotUtil.takeSnapshot("test_login.jpeg");

	}

	//@Test (priority = 0) 
	public void test_verifyJiraHomePage() { 
		LoginPage loginPage = new LoginPage(); 
		loginPage.waitForPageToLoad();
		loginPage.signOnLoginPage(); 

		SnapshotUtil.takeSnapshot("JiraPage.jpeg");

	}

	//@Test (priority = 0) 
	public void test_selectTimesheet() { 
		
		String startDate = "";
		String enddate = "";

		ConfigureMyTimesheetPage timesheetpage = new ConfigureMyTimesheetPage();
		timesheetpage.navigateToMyTimesheet();
		timesheetpage.selectProject("WF_Mobile_Banking");
		getDriver().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS); 
		//enter start and end date
		timesheetpage.getTextboxStartDate().sendKeys(getPropertiesFile().getStartDate
				().toString()); 
		getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		timesheetpage.getTextboxEndDate().sendKeys(getPropertiesFile().getEndDate().
				toString()); 
		getDriver().manage().timeouts().implicitlyWait(1,
				TimeUnit.SECONDS); 
		SnapshotUtil.takeSnapshot("BeforeNextbtn.jpeg");
		timesheetpage.getButtonNext().click();
		getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		SnapshotUtil.takeSnapshot("SelectedProject.jpeg");


	}



	//**************** WORKING DRIVER APRIL $ 2 pm
	/*
	 * @Test (priority = 0) 
	public void testLogin() { 
		LoginPage loginPage = new LoginPage(driver); 
		System.out.println("PAGE INSTANTIATIONS"); 
		}
	 */

}
