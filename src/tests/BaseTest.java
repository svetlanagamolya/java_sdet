package tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import bsh.Capabilities;
import pages.BasePage;
import utils.PropertiesFileUtils;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;


public class BaseTest {
	
	private static WebDriver driver;
	protected static BasePage basePage;
	
	public static PropertiesFileUtils getPropertiesFile() {
		PropertiesFileUtils loadFile = new PropertiesFileUtils();
		loadFile.loadPropertiesFile();
		return loadFile;
	}
	
	private static final String APP_URL = getPropertiesFile().getURL().toString();
	 
	private static void browserNavigate(String APP_URL) { 
		ChromeOptions options  = new ChromeOptions();
        //options.addArguments("--disable-popup-blocking");
        options.setCapability("disable-popup-blocking", true);
        setDriver(new ChromeDriver(options));
		getDriver().manage().window().maximize();
        getDriver().manage().deleteAllCookies(); 
        if (APP_URL instanceof String ){
			getDriver().get(APP_URL);
		} 
		else { 
			System.err.println("Error: url must be a string"); } 
        }
	
	//@BeforeClass
	//@BeforeMethod
	//@BeforeTest
	@BeforeSuite
    public static void setup () {
		System.out.println("Setting up driver");
		setChromeDriverProperty();
		browserNavigate(APP_URL); 
		basePage = new BasePage();
		basePage.setWebDriver(getDriver());
		
    }
 
    //@AfterClass
	//@AfterTest
	@AfterSuite
    public void teardown () {
        //driver.quit();
    	getDriver().manage().deleteAllCookies();
		System.out.println("teardown driver"); 
		if(null != getDriver()) {
			getDriver().close(); 
			getDriver().quit(); } 
    }
    
    
    private static void setChromeDriverProperty(){
		System.setProperty("webdriver.chrome.driver",
				"/Users/infoneer31infostretch/eclipse-workspace/Java_test/chrome_driver/chromedriver_80");
		
		}
    
    private static void setChromeCapabilities(){
    	
		
		}

	public static WebDriver getDriver() {
		return driver;
	}

	public static void setDriver(WebDriver driver) {
		BaseTest.driver = driver;
	}
   
	
	 
    
    //**************** WORKING DRIVER APRIL 4 2pm
    /*
     * public static WebDriver driver;
	private static final String APP_URL = "https://jira.infostretch.com";
	 
	private static void browserNavigate(String APP_URL) { 
		driver = new ChromeDriver();
		driver.manage().window().maximize();
        driver.manage().deleteAllCookies(); 
        if (APP_URL instanceof String ){
			driver.get(APP_URL);
		} 
		else { 
			System.err.println("Error: url must be a string"); } 
        }
	
	//@BeforeClass
	@BeforeMethod
    public void setup () {
		setChromeDriverProperty();
		browserNavigate(APP_URL);       
    }
 
    @AfterClass
    public void teardown () {
        //driver.quit();
    	driver.manage().deleteAllCookies();
		System.out.println("inside afterClass"); 
		if(null != driver) {
			driver.close(); 
			driver.quit(); } 
    }
    
    
    private static void setChromeDriverProperty(){
		System.setProperty("webdriver.chrome.driver",
				"/Users/infoneer31infostretch/eclipse-workspace/Java_test/chrome_driver/chromedriver_80");
		}
     */
    
    //***************************************
    
    //*****************************WORKING driver April 4 11 AM
    /*public WebDriver driver;
	 
    @BeforeClass
    public void setup () {
        //Create a Chrome driver. All test classes use this.
        System.setProperty("webdriver.chrome.driver", "/Users/infoneer31infostretch/eclipse-workspace/Java_test/chrome_driver/chromedriver_80");
        driver = new ChromeDriver();
 
        //Maximize Window
        driver.manage().window().maximize();
    }
 
    @AfterClass
    public void teardown () {
        driver.quit();
    }
     * 
     */

}
