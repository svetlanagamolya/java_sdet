package tests;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import components.LogWorkComponent;
import components.MoreComponent;
import pages.IssuePage;
import pages.LoginPage;
import utils.CommonUtils;
import utils.SnapshotUtil;

public class IssueTests extends BaseTest{

	public IssueTests() {
		// TODO Auto-generated constructor stub
	}
	
	//@Test (priority = 0) 
		public void test_selectTimesheet() { 
		
		LoginPage loginPage = new LoginPage();
		loginPage.signOnLoginPage();
		
		IssuePage issuePage = new IssuePage();
		issuePage.waitForPageToLoad();
		//@TODO add assertion
		issuePage.getHeader().getText().contains("WFMB-1263");
		issuePage.getButtonMore().click();
		
		MoreComponent menuMore = new MoreComponent();
		menuMore.waitForPageToLoad();
		menuMore.getLinkLogWork().click();
		
		LogWorkComponent logwork = new LogWorkComponent();
		logwork.waitForPageToLoad();
		logwork.getTextboxTimeSpent().sendKeys("8h");
		
		logwork.getCalendarIcon().click();
		logwork.waitForCalendarToLoad();
		
		logwork.selectDate("15");
		//getDriver().manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS); 
		//logwork.selectDate("12");
		
		getDriver().manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS); 
		SnapshotUtil.takeSnapshot("ClosedCalendar.jpeg");
			
		}

	@Test (priority = 0) 
	public void test_selectDate() { 
	
		
		  LoginPage loginPage = new LoginPage(); 
		  loginPage.signOnLoginPage();
		  
		 /* IssuePage issuePage = new IssuePage(); issuePage.waitForPageToLoad(); //@TODO
		 * add assertion issuePage.getHeader().getText().contains("WFMB-1263");
		 * issuePage.getButtonMore().click();
		 * 
		 * MoreComponent menuMore = new MoreComponent(); menuMore.waitForPageToLoad();
		 * menuMore.getLinkLogWork().click();
		 * 
		 * LogWorkComponent logwork = new LogWorkComponent();
		 * logwork.waitForPageToLoad(); logwork.getTextboxTimeSpent().sendKeys("8h");
		 * 
		 */
		LogWorkComponent logwork = new LogWorkComponent();
		logwork.fillInLogWorkForMonth();
	
	}
}
