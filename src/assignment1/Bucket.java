package assignment1;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Bucket {

	static ArrayList<BucketUnit> bucket = new ArrayList<BucketUnit>();

	private static double round(double value) {
		DecimalFormat df = new DecimalFormat("#.00");
		value = Double.valueOf(df.format(value));
		return value;
	}

	static void addUnit(BucketUnit unit) {

		bucket.add(unit);

	}

	static double getTotalPrice() {
		double res = 0;
		for (int i = 0; i < bucket.size(); i++) {

			/*
			 * 0 - Books; 1- CD; 2- Cosmetics
			 */

			int tax = 0;
			if (bucket.get(i).unitType == 1) {
				if (bucket.get(i).isImported)
					tax = 10 + 5;
				else
					tax = 10;
			} else if (bucket.get(i).unitType == 2) {
				tax = 10;
			}

			res = round(
					res + bucket.get(i).qty * (bucket.get(i).unitPrice + round(bucket.get(i).unitPrice * tax / 100)));
			// System.out.println("iteration " + i);
			// System.out.println("price = " + res);
			// System.out.println("price new= "
			// + bucket.get(i).qty * (bucket.get(i).unitPrice +
			// round(bucket.get(i).unitPrice * tax / 100)));

		}

		return res;
	}

	public static void main(String[] args) {

		BucketUnit currentBucketUnit = new BucketUnit(0, 2, "The Adventures of Tom Sawyer", 10.99, false);
		addUnit(currentBucketUnit);
		currentBucketUnit = new BucketUnit(0, 1, "The Adventures of Tom Sawyer", 12.99, false);
		addUnit(currentBucketUnit);
		currentBucketUnit = new BucketUnit(1, 10,
				"1991 QUEEN - THE SHOW MUST GO ON ENGLAND CD SINGLE BOXSET PARLOPHONE", 25, false);
		addUnit(currentBucketUnit);
		currentBucketUnit = new BucketUnit(1, 2, "1991 QUEEN - THE SHOW MUST GO ON ENGLAND CD SINGLE BOXSET PARLOPHONE",
				20, true);
		addUnit(currentBucketUnit);
		currentBucketUnit = new BucketUnit(2, 3,
				"32pcs Professional Soft Cosmetic Eyebrow Shadow Makeup Brush Set Kit +Pouch Case", 9.99, true);
		addUnit(currentBucketUnit);

		System.out.println("total price = " + getTotalPrice());

	}

}