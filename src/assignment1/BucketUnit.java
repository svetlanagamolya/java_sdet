package assignment1;

import java.time.Instant;

/*A company named XYZ Retail is in the business of selling Books, CDs and Cosmetics. 
 * Books are sales tax exempt and CDs and Cosmetics have a sales tax of 10%.
 * CDs can be imported and attracts an import tax of 5%
 * 
 * Write a simple shopping basket program, 
 * which will calculate extended price (qty * (unitprice + tax))
 * inclusive of tax for each item in the basket?
*/

public class BucketUnit {

	int unitType;
	int qty;
	String unitName;
	double unitPrice;
	Instant createdDateTime;
	boolean isImported;

	public BucketUnit(int unitType, int qty, String unitName, double unitPrice, boolean isImported) {
		this.unitType = unitType;
		this.qty = qty;
		this.unitName = unitName;
		this.unitPrice = unitPrice;
		this.isImported = isImported;
		this.createdDateTime = java.time.Clock.systemUTC().instant();

	}

}