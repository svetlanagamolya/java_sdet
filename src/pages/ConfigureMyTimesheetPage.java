package pages;

import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import components.TimesheetComponent;
import utils.CommonUtils;
import utils.SnapshotUtil;
import org.openqa.selenium.support.ui.Select;

public class ConfigureMyTimesheetPage extends BasePage{

	public ConfigureMyTimesheetPage() {
		// TODO Auto-generated constructor stub
	}
	public WebElement getHeader(){
		WebElement header = driver.findElement(By.xpath("//*[@id='content']/header/div/div/h1[contains(text(), 'Configure - My Timesheet')]"));
		return header;
	}

	public WebElement getTextStartDate(){
		WebElement textStartDate = driver.findElement(By.xpath("//*[@id='content']/header/div/div/h1[contains(text(), 'Configure - My Timesheet')]"));
		return textStartDate;
	}
	
	public WebElement getTextboxStartDate(){
		WebElement textboxStartDate = driver.findElement(By.xpath("//input[@id='date_startDate']"));
		return textboxStartDate;
	}
	public WebElement getTextboxEndDate(){
		WebElement textboxEndDate = driver.findElement(By.xpath("//input[@id='date_endDate']"));
		return textboxEndDate;
		
	}
	
	public WebElement getButtonNext(){
		WebElement buttonNext = driver.findElement(By.xpath("//input[@id='next_submit']"));
		return buttonNext;
		
	}
	
	

	public void waitForPageToLoad() { 
		waitForPageToLoad(getHeader()); 
		Reporter.log(this.getClass().getName()+" loaded");
		SnapshotUtil.takeSnapshot(this.getClass().getName()+CommonUtils.getDTString()+".jpeg");
	}

	public void navigateToMyTimesheet() {

		MainPage mainpage = new MainPage();
		mainpage.loginJira();
		TimesheetComponent timesheetcomponent = new TimesheetComponent();
		timesheetcomponent.getButtonTimesheet().click();
		timesheetcomponent.waitForPageToLoad();
		timesheetcomponent.getLinkMyTimesheet().click();

		this.waitForPageToLoad();

	}


	public void selectProject(String project) {
		Select projects = new Select (driver.findElement(By.xpath("//*[@id='projectId_select']")));
		
		try {
			projects.selectByVisibleText(project);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		 * for(WebElement e : CommonUtils.getListItems("//*[@id='projectId_select']")) {
		 * System.out.println("looking for project: "+project); Select projectitem =new
		 * Select(e);
		 * 
		 * if (e.getText()==project) { //e.click();
		 * projectitem.selectByVisibleText(project);
		 * System.out.println("Selected project: "+e.getText()); }
		 * System.out.println("PROJECT NOT SELECTED!!!"); }
		 */
	}





}
