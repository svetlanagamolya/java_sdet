package pages;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.BaseTest;
import utils.PropertiesFileUtils;

public class BasePage {



	protected static WebDriver driver;



	public void setWebDriver(WebDriver driver) {
		BasePage.driver=driver;
	}

	protected static WebDriverWait wait = new WebDriverWait(BaseTest.getDriver(), Duration.ofSeconds(60));



	
	/*
	 * wait for visibility of element
	 */

	protected void waitForPageToLoad(WebElement elementToWait) {
		wait.until(ExpectedConditions.visibilityOf(elementToWait)); 
	}
	
	protected void waitForElementInvisible(WebElement elementToWait) {
		wait.until(ExpectedConditions.invisibilityOf(elementToWait)); 
	}

	
	
	protected boolean verifyIfNotDisplayed(WebElement webelement) {
		try {
			return (!webelement.isDisplayed());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
			
		}
	}
		

	/* 
	 * method loads and reads data.properties file and 
	 */
	public PropertiesFileUtils getPropertiesFile() {
		PropertiesFileUtils loadFile = new PropertiesFileUtils();
		loadFile.loadPropertiesFile();
		return loadFile;
	}
	/* some base methods 
	 * 
	 * To check Element Present:

if(driver.findElements(By.xpath("value")).size() != 0){
System.out.println("Element is Present");
}else{
System.out.println("Element is Absent");
}
Or

if(driver.findElement(By.xpath("value"))!= null){
System.out.println("Element is Present");
}else{
System.out.println("Element is Absent");
}
To check Visible:

if( driver.findElement(By.cssSelector("a > font")).isDisplayed()){
System.out.println("Element is Visible");
}else{
System.out.println("Element is InVisible");
}
To check Enable:

if( driver.findElement(By.cssSelector("a > font")).isEnabled()){
System.out.println("Element is Enable");
}else{
System.out.println("Element is Disabled");
}
To check text present

if(driver.getPageSource().contains("Text to check")){
System.out.println("Text is present");
}else{
System.out.println("Text is absent");
}
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	//Wait Wrapper Method
	public void waitVisibility(By elementBy) {
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementBy));
	}

	//Click Method
	public void click (By elementBy) {
		waitVisibility(elementBy);
		driver.findElement(elementBy).click();
	}

	//Write Text
	public void writeText (By elementBy, String text) {
		waitVisibility(elementBy);
		driver.findElement(elementBy).sendKeys(text);
	}

	//Read Text
	public String readText (By elementBy) {
		waitVisibility(elementBy);
		return driver.findElement(elementBy).getText();
	}

	//Assert
	public void assertEquals (By elementBy, String expectedText) {
		waitVisibility(elementBy);
		Assert.assertEquals(readText(elementBy), expectedText);

	}
	 */

	//************************ Working DRIVER April 4 2 PM

	/*
	 * public WebDriver driver;
    public WebDriverWait wait;

    //Constructor
    public BasePage (WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver,15);
    }

    //Wait Wrapper Method
    public void waitVisibility(By elementBy) {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementBy));
    }

    //Click Method
    public void click (By elementBy) {
        waitVisibility(elementBy);
        driver.findElement(elementBy).click();
    }

    //Write Text
    public void writeText (By elementBy, String text) {
        waitVisibility(elementBy);
        driver.findElement(elementBy).sendKeys(text);
    }

    //Read Text
    public String readText (By elementBy) {
        waitVisibility(elementBy);
        return driver.findElement(elementBy).getText();
    }

    //Assert
    public void assertEquals (By elementBy, String expectedText) {
        waitVisibility(elementBy);
        Assert.assertEquals(readText(elementBy), expectedText);

    }
	 */

}
