package pages;

import java.time.LocalDateTime;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import utils.CommonUtils;
import utils.PropertiesFileUtils;
import utils.SnapshotUtil;


public class LoginPage extends BasePage{





	public WebElement getHeader(){
		WebElement header = driver.findElement(By.xpath("//h1[contains(text(), 'System Dashboard')]"));
		return header;
	}

	public WebElement getLinkLogIn(){
		WebElement linkLogIn = driver.findElement(By.xpath("//*[@id='user-options']/a"));
		return linkLogIn;
	}

	public WebElement getLogo(){
		WebElement logo = driver.findElement(By.id("logo"));
		return logo;
	}

	public WebElement getTextbox_username(){
		WebElement textbox_username = driver.findElement(By.id("login-form-username"));
		return textbox_username;
	}

	public WebElement getTextbox_password(){
		WebElement textbox_password = driver.findElement(By.id("login-form-password"));
		return textbox_password;
	}
	/*
	 * adding OR to handle LogIn button on Jira Main Page and on Login page via parametarized link
	 */
	public WebElement getButton_login(){
		WebElement button_login = driver.findElement(By.xpath("//*[@id='login-form-submit']|//*[@id='login']"));
		//*[@id="login-form-submit"]|//*[@id="login"]
		//*[@id="login"]
		return button_login;
	}

	public WebElement getError_invalidLogin(){
		WebElement button_login = driver.findElement(By.xpath("//div[@id='usernameerror']/p[contains(text(), 'Sorry, your username and password are incorrect - please try again.')]"));
		return button_login;
	}


	//*[@id="usernameerror"]

	/*//div[@id="usernameerror"]/p[contains(text(), 'Sorry, your username and password are incorrect - please try again.')]
	 */

	/*
	 * @FindBy(id = "logo") public WebElement logo;
	 */

	/*
	 * @FindBy(id = "login-form-username") public WebElement textbox_username;
	 * 
	 * @FindBy(id = "login-form-password") public WebElement textbox_password;
	 * 
	 * @FindBy(id = "login") public WebElement button_login;
	 */



	public void signOnLoginPage() {
		this.waitForPageToLoad();
		this.getTextbox_username().sendKeys(getPropertiesFile().getUsername().toString());
		this.getTextbox_password().sendKeys(getPropertiesFile().getPassword().toString());
		this.getButton_login().click();
	}


	public void waitForPageToLoad() { 
		waitForPageToLoad(this.getLinkLogIn()); 
		Reporter.log(this.getClass().getName()+" loaded<br>");
		SnapshotUtil.takeSnapshot(this.getClass().getName()+CommonUtils.getDTString()+".jpeg");
	}




	//***************** WORKING DRIVER APRIL 4 2 pm
	/*
	 * public LoginPage(WebDriver driver) {
          super(driver);
  		// TODO Auto-generated constructor stub
  	}
	 */


}
