package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import utils.CommonUtils;
import utils.SnapshotUtil;

public class IssuePage extends BasePage{

	public IssuePage() {
		// TODO Auto-generated constructor stub
	}
	
	//*[@id="stalker"]/div/header[contains(text(), 'WFMB-1263')]

	
	public WebElement getHeader(){
		WebElement header = driver.findElement(By.xpath("//header[@class='aui-page-header']/div//h1[@id='summary-val' and text()='4387 - Zelle4']"));
		return header;
	}
	
	//*[@id='opsbar-operations_more']
	
	public WebElement getButtonMore(){
		WebElement buttonMore = driver.findElement(By.xpath("//*[@id='opsbar-operations_more']"));
		return buttonMore;
	}
	
	public WebElement getTextNoDataAvailable(){
		WebElement textNoDataAvailable = driver.findElement(By.xpath("//table/tbody/tr[@class='no-test-scenario-row']/td[text()='No data available']"));
		return textNoDataAvailable;
	}
	
	//test
	public WebElement getTestLocator(){
		WebElement testLocator = driver.findElement(By.xpath("//html/body/div[1]/div[3]/div/table/tbody[1]/tr"));
		return testLocator;
	}
	
	public WebElement getLinkLogWork(){
		WebElement linkLogWork = driver.findElement(By.xpath("//*[@id='log-work-link']"));
		return linkLogWork;
	}
	public void waitForPageToLoad() { 
		waitForPageToLoad(this.getHeader());
		//waitForPageToLoad(this.getTextNoDataAvailable());
		//driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS); 
		Reporter.log(this.getClass().getName()+" loaded<br>");
		SnapshotUtil.takeSnapshot(this.getClass().getName()+CommonUtils.getDTString()+".jpeg");
	}
	
	//*[@id="opsbar-operations_more_drop"]
	public void selectMoreOption(String option) {
		Select projects = new Select (driver.findElement(By.xpath("//*[@id='projectId_select']")));
		
		try {
			projects.selectByVisibleText(option);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
}
